/* File: gulpfile.js */
const gulp = require('gulp'),
  uglify = require('gulp-uglify'),
  jshint = require('gulp-jshint');

// define the default task and add the watch task to it
gulp.task('default', ['watch']);

// configure the jshint task
gulp.task('jshint', function() {
  return gulp.src('src/assets/scripts/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('build', function(done){
  gulp.src('src/*.html')
  .pipe(gulp.dest('public'));
  done();
});

gulp.task('minify', function(done){
  gulp.src('src/assets/scripts/*.html')
  .pipe(uglify())
  .pipe(gulp.dest('public/assets/scripts'));
  done();
});

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
  gulp.watch('src/assets/scripts/**/*.js', ['jshint']);
});

gulp.task('autoprefixer', () => {
  const autoprefixer = require('autoprefixer')
  const sourcemaps = require('gulp-sourcemaps')
  const postcss = require('gulp-postcss')

  return gulp.src('./src/*.css')
    .pipe(sourcemaps.init())
    .pipe(postcss([ autoprefixer() ]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dest'))
})