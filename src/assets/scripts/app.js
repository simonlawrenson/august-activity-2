/****************************************
 * Start: Page Fade In Animation
 * Event: When content is loaded,
 * remove opacity from body &
 * add class to fade in content
****************************************/
const body = document.querySelector('body');
document.addEventListener('DOMContentLoaded', () => {
  body.classList.remove('hidden');
  body.classList.add('fade-in');
});
/****************************************
 * Finish: Page Fade In Animation
****************************************/

/****************************************
 * Start: Open and Close Responsive Navigation
 * Event: Click on responsive burger menu
****************************************/
const nav = document.querySelector('.header__nav'),
      navToggler = document.querySelector('.header__menu__btn'),
      hamburger = document.querySelector('.hamburger');

  let isNavExpanded = navToggler.getAttribute('aria-expanded');

navToggler.addEventListener('click', e => {
  e.preventDefault();
  e.stopImmediatePropagation();
  
  /* If the navigation is expanded, close it */
  if (isNavExpanded === 'true') {
    closeNavigation();
  } else {
    expandNavigation();
  }
});

/****************************************
 * Expand Navigation
 * add class nav-is-open
 * change aria labels to true
 ****************************************/
function expandNavigation() {
  nav.classList.add('nav-is-open');
  navToggler.setAttribute('aria-expanded', true);
  navToggler.setAttribute('aria-label', 'Close menu');

  return isNavExpanded = 'true';
}

/****************************************
 * Close Navigation
 * remove class nav-is-open
 * change aria labels to false
 ****************************************/
function closeNavigation() {
  nav.classList.remove('nav-is-open');
  navToggler.setAttribute('aria-expanded', false);
  navToggler.setAttribute('aria-label', 'Show menu');
  
  return isNavExpanded = 'false';
}
/****************************************
 * Finish: Open and Close Responsive Navigation
****************************************/

/****************************************
 * Start: Add class to header on scroll
 * Event: On scroll add background to the header
* Condition: If element already scrolled on load, add class
****************************************/
const header = document.getElementById('header');

// OnScroll event handler
const headerBackground = () => {
  // Get scroll value
  const scroll = document.documentElement.scrollTop
  // If scroll value is more than 0 - add class
  if (scroll > 0) {
    header.classList.add('is-scrolled');
  } else {
    header.classList.remove('is-scrolled');
  }
};

document.addEventListener('DOMContentLoaded', (e) => {
  headerBackground();
  window.addEventListener('scroll', headerBackground);
});
/****************************************
 * Finish: Add class to header on scroll
****************************************/

/****************************************
 * Section Slide In Animation
 * Event: On element enters viewport
 * add class and animate in
 * Condition: If element already above on load, add class
****************************************/
const scrollSlideInAnimation = () => {
  const sections = document.querySelectorAll('.slide-in');
  if (!document.querySelectorAll('.slide-in:not(.visible)')) return;

  for (const section of sections) {
    if (section.getBoundingClientRect().top <= window.innerHeight * 0.75 || section.getBoundingClientRect().top === 0 ) {
      section.classList.add('visible');
    }
  }
};

document.addEventListener('DOMContentLoaded', (e) => {
  scrollSlideInAnimation();
  window.addEventListener('scroll', scrollSlideInAnimation);
});
/****************************************
 * Start: Section Slide In Animation
****************************************/