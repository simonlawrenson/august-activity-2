# README #

This is the FED Practical Activity 3 for August. 

## FED Practical ##

### How do I get set up? ###

### `npm install`
Install scripts

## Available Scripts

### `npm start`

Runs a development server.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />


### Contact? ###

* [Simon Lawrenson](https://lawrenson.dev)
* [hello@lawrenson.dev](mailto:hello@lawrenson.dev)
