/****************************************
 * Register Event Listeners
 * Register Event Listeners
****************************************/
/****************************************
 * Open and Close Responsive Navigation
****************************************/
const nav = document.querySelector('.header__nav'),
      navToggler = document.querySelector('.header__menu__btn'),
      hamburger = document.querySelector('.hamburger');

      let isNavExpanded = navToggler.getAttribute('aria-expanded');

navToggler.addEventListener('click', e => {
  e.preventDefault();
  e.stopImmediatePropagation();
  
  if (isNavExpanded === 'true') {
    closeNavigation();
  } else {
    openNavigation();
  }
});

/****************************************
 * Open Navigation
 * add class nav-is-open
 * change aria labels to true
 ****************************************/
function openNavigation() {
  nav.classList.add('nav-is-open');
  navToggler.setAttribute('aria-expanded', true);
  navToggler.setAttribute('aria-label', 'Close menu');

  return isNavExpanded = 'true';
}
/****************************************
 * Close Navigation
 * remove class nav-is-open
 * change aria labels to false
 ****************************************/
function closeNavigation() {
  nav.classList.remove('nav-is-open');
  navToggler.setAttribute('aria-expanded', false);
  navToggler.setAttribute('aria-label', 'Show menu');
  
  return isNavExpanded = 'false';
}


/****************************************
 * Page Fade In Animation
****************************************/
const body = document.querySelector('body');

document.addEventListener('DOMContentLoaded', () => {
  body.classList.remove('hidden');
  body.classList.add('fade-in');
  const pageLinks = document.links;

  // Object.keys(pageLinks).forEach((key,i) => {
  //   pageLinks[key].addEventListener('click', e => {
  //     body.classList.add('fade-out');
  //   });
  // });
});

/****************************************
 * Add class to header on scroll
****************************************/
const header = document.getElementById('header');
// OnScroll event handler
const onScroll = () => {
  // Get scroll value
  const scroll = document.documentElement.scrollTop
  // If scroll value is more than 0 - add class
  if (scroll > 0) {
    header.classList.add('is-scrolled');
  } else {
    header.classList.remove('is-scrolled');
  }
}

window.addEventListener('scroll', onScroll);


/****************************************
 * Section Slide In Animation
****************************************/
document.addEventListener('DOMContentLoaded', () => {
  addAnimation();  
  window.onscroll = () => {
    addAnimation();
  }
});

function addAnimation() {
  const sections = document.querySelectorAll('.slide-in');
  if (!document.querySelectorAll('.slide-in:not(.visible)')) return;

  for (const section of sections) {
    if (section.getBoundingClientRect().top <= window.innerHeight * 0.75 && section.getBoundingClientRect().top > 0) {
      section.classList.add('visible');
    }
  }
}